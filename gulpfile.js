
// Gulp File

const gulp = require("gulp");
const sass = require('gulp-sass');
const minify = require('gulp-minify');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const rigger = require('gulp-rigger');
const rename = require("gulp-rename");






const patchs = {
	cleanCSS: {
		src: "./dist/*",
		// dist: "./dist"
	},
	html: {
		src: "./src/templates/index.html",
		srcTemplates:"./src/templates/*.html",
		dist: "./dist"
	},
	scss: {
		src: "./src/scss/**/*.scss",
		dist: "./dist/css/"
	},
	js: {
		src: [
			"./src/js/vendor/*.js",
			"./src/js/*.js",
		],

		dist: "./dist/js/"
	},
	img: {
		src: "./src/img/**/*.{jpeg,png,webp,gif,svg}",
		dist: "./dist/img/"
	}


};


const moveCLEAN = () =>
	gulp.src(patchs.cleanCSS.src, { read: false }).
		pipe(clean());

// pipe(gulp.dest(patchs.cleanCSS.dist));

const moveSCSS = () =>
	gulp.src(patchs.scss.src).
		pipe(sass({ outputStyle: 'compressed' })).
		pipe(concat('style.min.css')).
		pipe(autoprefixer({
			overrideBrowserslist: ['last 2 versions'],
			cascade: false
		})).
		pipe(gulp.dest(patchs.scss.dist)).
		pipe(browserSync.stream());

const moveJS = () =>
	gulp.src(patchs.js.src).
		pipe(uglify()).
		pipe(rename(function (path) {							
			path.extname = ".min.js";
		})).

		pipe(gulp.dest(patchs.js.dist)).
		pipe(browserSync.stream());

const moveIMG = () =>
	gulp.src(patchs.img.src).
		pipe(imagemin()).
		pipe(gulp.dest(patchs.img.dist)).
		pipe(browserSync.stream());

const moveTHML = () =>
	gulp.src(patchs.html.src).
		pipe(rigger()).
		pipe(gulp.dest(patchs.html.dist)).
		pipe(browserSync.stream());

const watch = () => {
	browserSync.init({
		server: {
			baseDir: './dist/'
		}
	});
	gulp.watch(patchs.scss.src, moveSCSS);
	gulp.watch(patchs.js.src, moveJS);
	gulp.watch(patchs.html.src, moveTHML);
	gulp.watch(patchs.html.srcTemplates, moveTHML);
	gulp.watch(patchs.img.src, moveIMG);
	gulp.watch('./*.index.html', browserSync.reload);
}

gulp.task('build', gulp.series(moveCLEAN, gulp.parallel(moveSCSS, moveTHML, moveIMG, moveJS)));

gulp.task('dev', gulp.series('build', watch));




